import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import './components/styles/main.scss';
import './assets/fonts/fonts.scss';
import WebFont from "webfontloader";

WebFont.load({
  google: {
    families: ["Oxygen:300,400,700", "Open Sans"]
  }
});

import App from './components/App'

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById("root")
  );
};

render(App);

if (module.hot) {
  module.hot.accept(App, () => {
    const App = require('./components/App');
    render(App);
  });
}

